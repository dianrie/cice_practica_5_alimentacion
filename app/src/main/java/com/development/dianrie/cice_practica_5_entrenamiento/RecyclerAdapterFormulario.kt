package com.development.dianrie.cice_practica_5_entrenamiento


import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class RecyclerAdapterFormulario(
    val arrayEmojis: Array<String>,
    val comunicador: FragmentFormularioImagenes.Comunicador?
) :
    RecyclerView.Adapter<RecyclerAdapterFormulario.ViewHolder>() {
    //Data Source

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerAdapterFormulario.ViewHolder {
        val vistaCelda = LayoutInflater.from(p0.context).inflate(R.layout.card_layout_formulario, p0, false)
        return ViewHolder(vistaCelda)
    }

    override fun getItemCount(): Int {
        return arrayEmojis.size
    }

    override fun onBindViewHolder(p0: RecyclerAdapterFormulario.ViewHolder, p1: Int) {
        p0.emoji.text = arrayEmojis[p1]
        p0.p1 = p1

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var emoji: TextView
        var intent = Intent(itemView.context, FormularioActivity::class.java)
        var p1: Int

        init {
            emoji = itemView.findViewById(R.id.textViewEmoji)
            p1 = 1

            //Celda Listener para pasar datos al formulario si se seleciona la celda
            itemView.setOnClickListener {
                //Extras
                Log.i("app", "Se ha seleccionado el emoji")
                comunicador?.pasarTextoAFormulariActivity(arrayEmojis[p1])


            }

        }
    }
}