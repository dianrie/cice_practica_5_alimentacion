package com.development.dianrie.cice_practica_5_entrenamiento

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

class RecyclerAdapterComida(val context: Context, val arrayId: Array<String>) :
    RecyclerView.Adapter<RecyclerAdapterComida.ViewHolder>() {

    val comida: Array<String> = arrayOf("desayuno", "comida", "merienda", "cena")

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerAdapterComida.ViewHolder {
        val vistaCelda = LayoutInflater.from(p0.context).inflate(R.layout.card_layout_menu, p0, false)
        return ViewHolder(vistaCelda)
    }

    override fun getItemCount(): Int {
        return arrayId.size
    }

    override fun onBindViewHolder(p0: RecyclerAdapterComida.ViewHolder, p1: Int) {
        p0.nombreComida.text = comida[p1]
        p0.nombreMenu.text = ComidaDataManager(context).readComida(arrayId[p1])
        p0.p1 = p1
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nombreComida: TextView
        var nombreMenu: TextView
        var intent = Intent(itemView.context, FormularioActivity::class.java)
        var p1: Int

        init {
            nombreComida = itemView.findViewById(R.id.textView)
            nombreMenu = itemView.findViewById(R.id.textView2)
            p1 = 1

            //Celda Listener para pasar datos a FormulariActuvity si se seleciona la celda
            itemView.setOnClickListener {
                Toast.makeText(itemView.context, "Elige ${comida[p1]}", Toast.LENGTH_SHORT).show()
                //Extras
                //Se envia como extra un array con los codigos para trabajar en share prefernces
                intent.putExtra("arrayId", arrayId)
                //Se envia el codigo para acceder a share preferences que coincide con la comida seleccionada(11,12,13,14 etc)
                intent.putExtra("codigoId", arrayId[p1])
                //Se envia el id de la comida que queremos modificar (desayuno,comida,merienda,cena)
                intent.putExtra("comidaId", comida[p1])

                itemView.context.startActivity(intent)

            }
        }
    }

}