package com.development.dianrie.cice_practica_5_entrenamiento

import android.content.Intent
import android.gesture.Gesture
import android.gesture.GestureLibraries
import android.gesture.GestureLibrary
import android.gesture.GestureOverlayView
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SeekBar
import android.widget.Toast


import kotlinx.android.synthetic.main.activity_menu_diario.*
import kotlinx.android.synthetic.main.content_menu_diario.*
import java.util.*

class MenuDiarioActivity : AppCompatActivity(), GestureOverlayView.OnGesturePerformedListener,
    SeekBar.OnSeekBarChangeListener {

    //Se crea un companion objer para mantener el dia seleccionado al cambiar de actividad
    companion object Dia {
        var numDia: Int = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_diario)
        setSupportActionBar(toolbarMenuDiario)

        //creo la variable para luego añadir los codigos del dia seleccionado
        var arrayId: Array<String>

        //Muestro en el toolbarMenuDiario el dia Seleccionado en Main Activity
        toolbarMenuDiario.title = "Menú"

        //Cargamos el archivo de gestos personalizados
        cargarGestos()

        //Creamos el Listener para el seekBar
        var seekBar = seekBar
        seekBar.setOnSeekBarChangeListener(this)

        //Recupero el array de id del dia que ha sido seleccionado en el menu o cojo el dia actual
        if (intent.getStringArrayExtra("arrayId") != null) {
            arrayId = intent.getStringArrayExtra("arrayId")
        } else {
            //Consulto cual es el numero del dia actual y consulto los codigos para el dia actual
            numDia = diaAcutal()
            arrayId = arrayDiaActual(numDia)
        }

        //Se calcula la barra de progreso y se rellena segun si las comidas estan completas
        val datos = ComidaDataManager(this)
        for (i in arrayId) {
            if (datos.readComida(i) != "\uD83C\uDF7D") {
                progressBar.progress += 1
            }

        }

        /**
         * RecyclerView -----------------------------------------
         *
         * Inicializamos le pasamos al RecyclerAdapter el contexto y un arrayid con las referencias a buscar
         * en el share preferences para mostrar las comidas del dia desayunco comida merienda cena
         */
        var layoutManager: RecyclerView.LayoutManager? = null
        var adapter: RecyclerView.Adapter<RecyclerAdapterComida.ViewHolder>? = null

        layoutManager = LinearLayoutManager(this)
        //Le paso al Recycler un array con los codigos para que saque de share preferneces la informacion necesaria
        adapter = RecyclerAdapterComida(this, arrayId)

        //Asignamos al RecyclerView el adapter
        miRecyclerView.layoutManager = layoutManager
        miRecyclerView.adapter = adapter
        //------------------------------------------------------
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_menu_diario, menu)
        //En el menu se muestra el dia seleccionado el primero fuera del menu
        menu.getItem(numDia).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        var intent = Intent(this, MenuDiarioActivity::class.java)
        return when (item.itemId) {
            R.id.lunes -> {
                intent.putExtra("arrayId", arrayOf("11", "12", "13", "14"))
                numDia = 0
                startActivity(intent)
                true
            }
            R.id.martes -> {
                intent.putExtra("arrayId", arrayOf("21", "22", "23", "24"))
                numDia = 1
                startActivity(intent)
                true
            }
            R.id.miercoles -> {
                intent.putExtra("arrayId", arrayOf("31", "32", "33", "34"))
                numDia = 2
                startActivity(intent)
                true
            }
            R.id.jueves -> {
                intent.putExtra("arrayId", arrayOf("41", "42", "43", "44"))
                numDia = 3
                startActivity(intent)
                true
            }
            R.id.viernes -> {
                intent.putExtra("arrayId", arrayOf("51", "52", "53", "54"))
                numDia = 4
                startActivity(intent)
                true
            }
            R.id.sabado -> {
                intent.putExtra("arrayId", arrayOf("61", "62", "63", "64"))
                numDia = 5
                startActivity(intent)
                true
            }
            R.id.domingo -> {
                intent.putExtra("arrayId", arrayOf("71", "72", "73", "74"))
                numDia = 6
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //-----------------GESTOS_PERSONALIZADOS------------------------------------------
    //Aquí vamos a cargar la info del archivo gestrues
    var gestureLibrary: GestureLibrary? = null

    override fun onGesturePerformed(overlay: GestureOverlayView?, gesture: Gesture?) {
        Log.i("Tag", "Gesto propio")

        val prediction = gestureLibrary?.recognize(gesture)
        Log.i("Tag", "Predicción es: $prediction")
        prediction?.let {
            Log.i("Tag", "Puntuación ${it[0]}: ${it[0].score}")


            if (it.size > 0 && it[0].score > 3.0) {
                //Toast
                Toast.makeText(this, "Gesto ${it[0].name} detectado", Toast.LENGTH_SHORT).show()
                onBackPressed()
            }

        }
    }

    //Función que carga los gestos personalizados del archivo gestures que esta en la carpeta raw
    private fun cargarGestos() {
        gestureLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures)
        if (gestureLibrary?.load() == false) {
            finish()
        }
        gestureOverlay.addOnGesturePerformedListener(this)
    }

    //Seguimiento valorSeekBar y segun su valor se cambia el color de AppBarLayout
    var valoSeekBar = 0

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        valoSeekBar = seekBar!!.progress
        Log.i("logapp", "Seekbar: $valoSeekBar")
        when (valoSeekBar) {
            in 0..5 -> {
                AppBarLayoutMenu.setBackgroundColor(getColor(R.color.colorPrimary))
            }
            in 6..25 -> {
                AppBarLayoutMenu.setBackgroundColor(Color.WHITE)
            }
            in 26..50 -> {
                AppBarLayoutMenu.setBackgroundColor(Color.BLUE)
            }
            in 51..75 -> {
                AppBarLayoutMenu.setBackgroundColor(Color.GRAY)
            }
            in 76..100 -> {
                AppBarLayoutMenu.setBackgroundColor(Color.DKGRAY)
            }
            else -> {
                seekBar!!.progress = 0
            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {}
    override fun onStopTrackingTouch(seekBar: SeekBar?) {}

    //al pulsar atras en la ActivigyMenuDiario Salir de la aplicación
    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    //funcion que devuelve el dia actual con un numero 0,1,2,3,4,5,6
    fun diaAcutal(): Int {
        val calendar = Calendar.getInstance()
        var diaIngles = calendar.get(Calendar.DAY_OF_WEEK)
        when (diaIngles) {
            1 -> {
                diaIngles = 6
            }
            2 -> {
                diaIngles = 0
            }
            3 -> {
                diaIngles = 1
            }
            4 -> {
                diaIngles = 2
            }
            5 -> {
                diaIngles = 3
            }
            6 -> {
                diaIngles = 4
            }
            7 -> {
                diaIngles = 5
            }
            else -> {
                diaIngles = 0
            }
        }

        return diaIngles

    }

    //Funcion que devuelve los codigos para trabagar con share preferences segun el dia que se le pasa
    fun arrayDiaActual(diaActual: Int): Array<String> {
        var arrayId: Array<String>

        when (diaActual) {
            0 -> {
                arrayId = arrayOf("11", "12", "13", "14")
            }
            1 -> {
                arrayId = arrayOf("21", "22", "23", "24")
            }
            2 -> {
                arrayId = arrayOf("31", "32", "33", "34")
            }
            3 -> {
                arrayId = arrayOf("41", "42", "43", "44")
            }
            4 -> {
                arrayId = arrayOf("51", "52", "53", "54")
            }
            5 -> {
                arrayId = arrayOf("61", "62", "63", "64")
            }
            6 -> {
                arrayId = arrayOf("71", "72", "73", "74")
            }
            else -> {
                arrayId = arrayOf("11", "12", "13", "14")
            }
        }
        return arrayId
    }
}
