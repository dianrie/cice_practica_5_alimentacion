package com.development.dianrie.cice_practica_5_entrenamiento


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.fragment_formulario_seekbar.*
import android.support.v4.view.GestureDetectorCompat
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.Toast



class FormularioActivity : AppCompatActivity(), FragmentFormularioImagenes.Comunicador,
    GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {


    //se implementa el metodo que perimite recibir informacion del FragmentFormularioImagenes y pasarla al FragmentFormularioSeekbar
    override fun pasarTextoAFormulariActivity(datos: String) {
        //Creo esta variable para poder acceder a los metodos del fragmento
        val fragmentFormularioSeekbar =
            supportFragmentManager.findFragmentById(R.id.fragment1) as FragmentFormularioSeekbar?
        if (fragmentFormularioSeekbar != null) {
            if (datos == "GUARDAR") {
                fragmentFormularioSeekbar.guardarTexto()
                //si se selecciona guardar se guarda y despues se llama a MenuDiario pasandole los coodigos recuperados del intent
                var arrayId = intent.getStringArrayExtra("arrayId")
                var intent = Intent(this, MenuDiarioActivity::class.java)
                intent.putExtra("arrayId", arrayId)
                intent.putExtra("NOMBRE_DIA", getString(R.string.lunes))
                startActivity(intent)

            } else {
                fragmentFormularioSeekbar.cambiarTexto(datos)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formulario)
        //Iniciamos el Gesture Detector
        gestureDetector = GestureDetectorCompat(this, this)
        //Aplicar el Doble tap Listener
        gestureDetector?.setOnDoubleTapListener(this)
    }

    //-----------------DETECTAR DOBLE TAP------------------------------------------
    var gestureDetector: GestureDetectorCompat? = null

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean {
        val fragmentFormularioSeekbar =
            supportFragmentManager.findFragmentById(R.id.fragment1) as FragmentFormularioSeekbar?
        fragmentFormularioSeekbar!!.textView4.text = ""
        Toast.makeText(this, "Doble Tap borra la Selección", Toast.LENGTH_SHORT).show()
        return true
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        gestureDetector?.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    override fun onDoubleTap(e: MotionEvent?): Boolean {
        return true
    }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
        return true
    }

    override fun onShowPress(e: MotionEvent?) {
    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return true
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return true
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        return true
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        return true
    }

    override fun onLongPress(e: MotionEvent?) {

    }
    //-----------------CONTROLAR VOTON ATRAS------------------------------------------
    //si se sale del formularioActivity se activa la variale pause con un 1
    var pause: Int = 0
    override fun onPause() {
        super.onPause()
        pause = 1
    }
    //Si le damos a retroceso despues de pasar por la activity Formulario volveremos al activity MainActivity
    override fun onResume() {
        super.onResume()
        Log.i("logapp", "pause: $pause")
        if (pause == 1) {
            var irMenuDiarioActivity = Intent(this, MenuDiarioActivity::class.java)
            startActivity(irMenuDiarioActivity)
        }
    }
}
