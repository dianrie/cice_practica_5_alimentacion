package com.development.dianrie.cice_practica_5_entrenamiento


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import kotlinx.android.synthetic.main.fragment_formulario_seekbar.*

class FragmentFormularioSeekbar : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        //Consulto la base de datos para ver que alimentos estan guardados y los muestro
        //si no hay ningun alimento previamente guardao no muestro nada

        val valor = ComidaDataManager(activity!!).readComida(activity!!.intent.getStringExtra("codigoId"))
        if (valor == "\uD83C\uDF7D") {
            textView4.text = ""
        } else {
            textView4.text = valor
        }

        //Al pulsar sobre el icono de borrar se borrala la seleccion actual
        borrar.setOnClickListener {
            textView4.text = ""
            Log.i("logapp", "La seleccion ha sido borrada")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_formulario_seekbar, container, false)
    }

    //Esta funcio es llamada desde FormularioActivity para modificar el texto con los datos del otro fragment
    var primeraVez = true

    fun cambiarTexto(datos: String) {
        //Si es la primera vez que se llama se borran los emoticonos que se muestran y se añade el seleccionado
        if (primeraVez) {
            primeraVez = false
            textView4.text = "$datos"
        } else {
            textView4.text = "${textView4.text}$datos"
        }

    }

    fun guardarTexto() {
        //Si se va a guardar sin ningun emoji se guarda por defecto el emoji del plato y el tenedor
        if (textView4.text == "") {
            ComidaDataManager(activity!!).saveComida(
                Pair(
                    activity!!.intent.getStringExtra("codigoId"),
                    "\uD83C\uDF7D"
                )
            )
        } else {
            ComidaDataManager(activity!!).saveComida(
                Pair(
                    activity!!.intent.getStringExtra("codigoId"),
                    "${textView4.text}"
                )
            )
        }
    }


}
