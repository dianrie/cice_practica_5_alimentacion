package com.development.dianrie.cice_practica_5_entrenamiento

import android.content.Context
import android.preference.PreferenceManager

/**
 * Este es el ComidaDataManager.
 *
 * Aqui se crean las funciones que permiten acceder a Share Preferences
 * saveComida para guardar pasandole un Pair<String,String>
 * readComida para recuperar informacion pasandole un id que es  un String
 * TABLA DE CODIGOS
 *          LUNES   MARTES  MIERCOLES   JUVES   VIERNES SABADO  DOMINGO
 * DESAYUNO 11      21      31          41      51      61      71
 * COMIDA   12      21      32          42      52      62      72
 * MERIENDA 13      23      33          43      53      63      73
 * CENA     14      24      34          44      54      64      74
 */
//EJEMPLOS
//val datos = ComidaDataManager(this)
//ComidaDataManager(this).readComida("11")
//datos.readComida("11")
//datos.saveComida(Pair("11","\ud83e\udd5b"))
//datos.saveComida(Pair("12","${datos.readComida("12")}\uD83C\uDF4F"))
//ComidaDataManager(this).saveComida(Pair("11","\ud83e\udd5b")

class ComidaDataManager(val context: Context) {
    //Guardar en Share Preferences
    //Preferences
    fun saveComida(comida: Pair<String, String>) {
        //Creo un set con los valores del MenuDay para poder almacenarlos como un set en share properties
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit()
        sharedPreferences.putString(comida.first, comida.second)
        sharedPreferences.apply()
    }

    //Recuperar en shared Preferences
    fun readComida(id: String): String {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val comida = sharedPreferences.getString("$id", "\uD83C\uDF7D")
        return comida

    }

}