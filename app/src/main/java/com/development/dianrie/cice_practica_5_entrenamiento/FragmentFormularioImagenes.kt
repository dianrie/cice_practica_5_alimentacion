package com.development.dianrie.cice_practica_5_entrenamiento


import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_formulario_imagenes.*


class FragmentFormularioImagenes : Fragment() {


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //Recuperar datos del intent que viene de el Recycler del Activity Menu
        var intent = activity!!.intent.getStringExtra("comidaId")
        textView5.text = "$intent"
        var arrayEmojis = DataEmojis().getEmojis(intent)

        /**
         * RecyclerView -----------------------------------------
         *
         * Inicializamos le pasamos al RecyclerAdapter el contexto y un arrayid con las referencias a buscar
         * en el share preferences para maostrar los emojis correspondientes
         */
        var layoutManager: RecyclerView.LayoutManager? = null
        var adapter: RecyclerView.Adapter<RecyclerAdapterFormulario.ViewHolder>? = null
        //
        layoutManager = GridLayoutManager(context, 3)
        adapter = RecyclerAdapterFormulario(arrayEmojis, comunicacion)

        //Asignamos al RecyclerView
        mIRecyclerViewFormulario.layoutManager = layoutManager
        mIRecyclerViewFormulario.adapter = adapter
        //------------------------------------------------------
        //Con ete fabButton confirmamos la seleccion y la guardamos despues vamos a MenuDiarioActivity
        fabButton.setOnClickListener { view ->
            fabButton.visibility = View.INVISIBLE
            Snackbar.make(view, "GUARDAR SELECCION", Snackbar.LENGTH_LONG)
                .setAction("OK") {
                    Toast.makeText(context, "Su seleccion ha sido guardada", Toast.LENGTH_SHORT).show()
                    actualizarTextoEnFagmentFormularioSeekbar("GUARDAR")
                }
                .setCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        if (event != Snackbar.Callback.DISMISS_EVENT_ACTION) {
                            // Close activity, only if snackbar was dismissed not pressing action button
                            fabButton.visibility = View.VISIBLE
                            //finish()
                        }
                    }
                })
                .show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var vista = inflater.inflate(R.layout.fragment_formulario_imagenes, container, false)
        // Inflate the layout for this fragment

        return vista
    }

    //Me aseguro de que el Formulario Activity implemente la interfaz de comunicacion
    //creo un Callback
    var comunicacion: Comunicador? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is Comunicador) {
            comunicacion = context

        } else {
            throw RuntimeException(context!!.toString() + " debe implementar Comunicador")
        }
    }

    fun actualizarTextoEnFagmentFormularioSeekbar(texto: String) {
        comunicacion?.pasarTextoAFormulariActivity(texto)
    }

    //Creo una interfar para implementarla en FormularioActivity para poder pasar informacion desde el fragment
    interface Comunicador {
        fun pasarTextoAFormulariActivity(datos: String)
    }

}
