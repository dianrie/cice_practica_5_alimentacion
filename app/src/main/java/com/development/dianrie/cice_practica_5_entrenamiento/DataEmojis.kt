package com.development.dianrie.cice_practica_5_entrenamiento

class DataEmojis {

    val desayuno: Array<String>
    val comida: Array<String>
    val merienda: Array<String>
    val cena: Array<String>

    init {
        desayuno = arrayOf(
            "\uD83E\uDD5B",
            "\uD83C\uDF75",
            "\u2615\ufE0F",
            "\uD83C\uDF6A",
            "\uD83C\uDF69",
            "\uD83C\uDF6B",
            "\uD83C\uDF6E",
            "\uD83E\uDD5E",
            "\uD83C\uDF6F",
            "\uD83E\uDD50",
            "\uD83E\uDD53",
            "\uD83C\uDF73"
        )
        comida = arrayOf(
            "\uD83E\uDD56",
            "\uD83C\uDF64",
            "\uD83C\uDF55",
            "\uD83C\uDF54",
            "\uD83C\uDF5D",
            "\uD83C\uDF70",
            "\uD83C\uDF5F",
            "\uD83C\uDF4F",
            "\uD83C\uDF50",
            "\uD83C\uDF4A",
            "\uD83C\uDF4C",
            "\uD83C\uDF53"
        )
        merienda = arrayOf(
            "\uD83C\uDF6B",
            "\uD83C\uDF68",
            "\uD83C\uDF82",
            "\uD83E\uDD5D",
            "\uD83C\uDF51",
            "\uD83C\uDF52",
            "\uD83C\uDF48",
            "\uD83C\uDF47"
        )
        cena = arrayOf(
            "\uD83E\uDD57",
            "\uD83C\uDF2E",
            "\uD83C\uDF2F",
            "\uD83C\uDF5C",
            "\uD83C\uDF72",
            "\uD83C\uDF71",
            "\uD83C\uDF5B",
            "\uD83C\uDF5A",
            "\uD83E\uDD53"
        )
    }

    fun getEmojis(comida1: String): Array<String> {
        when (comida1) {
            "desayuno" -> return desayuno
            "comida" -> return comida
            "merienda" -> return merienda
            "cena" -> return cena
            else -> return desayuno
        }
    }

}